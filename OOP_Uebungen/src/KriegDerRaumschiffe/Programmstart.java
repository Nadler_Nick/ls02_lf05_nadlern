package KriegDerRaumschiffe;

public class Programmstart {

	public static void main(String[] args) {
        
		Raumschiff klingonen = new Raumschiff(1, 100, 100, 100, 100, 2, "IKS Hegh'ta");
		Raumschiff romulaner = new Raumschiff(2, 100, 100, 100, 100, 2, "IRW Khazara");
		Raumschiff vulkanier = new Raumschiff(0, 80, 80, 50, 100, 5, "Ni'Var");
		


		Ladung ladungK1 = new Ladung("Ferengi Schneckensaft", 200);
		Ladung ladungK2 = new Ladung("Bat'leth Klingonen Schwert", 200);		
		Ladung ladungR1 = new Ladung("Borg-Schrott", 5);	
		Ladung ladungR2 = new Ladung("Rote Marterie", 2);
		Ladung ladungR3 = new Ladung("Plasma-Waffe", 50);
		Ladung ladungV1 = new Ladung("Forschungssonde", 35);
		Ladung ladungV2 = new Ladung("Photonentorpedo", 3);
		
		klingonen.addLadung(ladungK1);
		klingonen.addLadung(ladungK2);
		romulaner.addLadung(ladungR1);
		romulaner.addLadung(ladungR2);
		romulaner.addLadung(ladungR3);
		vulkanier.addLadung(ladungV1);
		vulkanier.addLadung(ladungV2);
		
		
		
		klingonen.photonentorpedoSchiessen("IRW Khazara");
		klingonen.logbuchAusgeben();
		System.out.println("");
		romulaner.phaserkanoneAbschießen("IKS Hegh'ta");
		romulaner.logbuchAusgeben();
		System.out.println("");
		vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch!");
		vulkanier.logbuchAusgeben();
		System.out.println("");
		klingonen.zustandRaumschiff();
		System.out.println("");
		
		klingonen.photonentorpedoSchiessen("IRW Khazara");
		klingonen.photonentorpedoSchiessen("IRW Khazara");
		System.out.println("");
		klingonen.zustandRaumschiff();
		klingonen.ladungAusgeben();
		System.out.println("");
		romulaner.zustandRaumschiff();
		romulaner.ladungAusgeben();
		System.out.println("");
		vulkanier.zustandRaumschiff();
		vulkanier.ladungAusgeben();


		
		
        

}

}
