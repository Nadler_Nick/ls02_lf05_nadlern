package KriegDerRaumschiffe;

import java.util.ArrayList;

public class Raumschiff {

	// Deklaration der Variablen

	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	public String nachricht;

	ArrayList<String> broadcastkommunikator = new ArrayList<String>();
	ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();

	// Konstruktor Methoden
	// ------------------------------------------------------------------------------------------

	public Raumschiff(int photonentorpedoAnzahl, int zustandEnergieversorgungInProzent, int zustandSchildeInProzent,
			int zustandHuelleInProzent, int zustandLebenserhaltungssystemeInProzent, int androidenAnzahl,
			String schiffsname) {

		setPhotonentorpedoAnzahl(photonentorpedoAnzahl);
		setEnergieversorgungInProzent(zustandEnergieversorgungInProzent);
		setSchildeInProzent(zustandSchildeInProzent);
		setHuelleInProzent(zustandHuelleInProzent);
		setLebenserhaltungssystemeInProzent(zustandLebenserhaltungssystemeInProzent);
		setAndroidenAnzahl(androidenAnzahl);
		setSchiffsname(schiffsname);

	}

	// Get - Setmethoden

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahlNeu) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahlNeu;
	}

	public int getPhotonentorpedoAnzahl() {
		return this.photonentorpedoAnzahl;
	}

	public void setEnergieversorgungInProzent(int zustandEnergieversorgungInProzentNeu) {
		this.energieversorgungInProzent = zustandEnergieversorgungInProzentNeu;
	}

	public int getEnergieversorgungInProzent() {
		return this.energieversorgungInProzent;
	}

	public void setSchildeInProzent(int zustandSchildeInProzentNeu) {
		this.schildeInProzent = zustandSchildeInProzentNeu;
	}

	public int getSchildeInProzent() {
		return this.schildeInProzent;
	}

	public void setHuelleInProzent(int zustandHuelleInProzentNeu) {
		this.huelleInProzent = zustandHuelleInProzentNeu;
	}

	public int getHuelleInProzent() {
		return this.huelleInProzent;
	}

	public void setLebenserhaltungssystemeInProzent(int zustandLebenserhaltungssystemeInProzentNeu) {
		this.lebenserhaltungssystemeInProzent = zustandLebenserhaltungssystemeInProzentNeu;
	}

	public int getLebenserhaltungssystemeInProzent() {
		return this.lebenserhaltungssystemeInProzent;
	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	public int getAndroidenAnzahl() {
		return this.androidenAnzahl;
	}

	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	public String getSchiffsname() {
		return this.schiffsname;
	}

	// Zustand des Raumschiffes

	public void zustandRaumschiff() {
		System.out.println("Schiffsame: " + this.getSchiffsname());
		System.out.println("Energie (%): " + this.getEnergieversorgungInProzent());
		System.out.println("Schild (%): " + this.getSchildeInProzent());
		System.out.println("Huelle (%): " + this.getHuelleInProzent());
		System.out.println("Lebenserhaltung (%): " + this.getLebenserhaltungssystemeInProzent());

	}

	// Torpedo abschießen

	public void photonentorpedoSchiessen(String Raumschiff) {
		if (photonentorpedoAnzahl > 0) {
			broadcastkommunikator.add("Photonentorpedo abgeschossen.");
			photonentorpedoAnzahl = photonentorpedoAnzahl - 1;
			treffer(Raumschiff);
			

		} else {
			System.out.println("-=*Click*=- ");
		}
	}

	// Treffer vermerken

	public void treffer(String Raumschiff) {
		broadcastkommunikator.add(Raumschiff + " wurde getroffen.");
		schildeInProzent = schildeInProzent - 50;

		if (schildeInProzent < 1) {

			huelleInProzent = huelleInProzent - 50;
			lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent - 50;

			if (huelleInProzent < 1) {

				broadcastkommunikator.add("Lebenserhaltungssysteme von " + Raumschiff + " wurden zerstört.");
			}
		}
	}

	public void taa(String Raumschiff) {
		System.out.println(photonentorpedoAnzahl);

	}
	// Phaserkanone abschießen

	public void phaserkanoneAbschießen(String Raumschiff) {
		if (energieversorgungInProzent > 50) {
			energieversorgungInProzent = energieversorgungInProzent - 50;
			broadcastkommunikator.add("Phaserkanone abgeschossen.");
			treffer(Raumschiff);

		}

		else {
			System.out.println("-=*Click*=- ");
		}

	}
	// Nachricht an Alle 
	public void nachrichtAnAlle(String nachricht) {
		broadcastkommunikator.add(nachricht);

	}
	
	//Ladung hinzufügen 
	public void addLadung(Ladung neueLadung) {
		ladungsverzeichnis.add(neueLadung);
	}

	//Logbuch ausgeben 
	public void logbuchAusgeben() {
		
		for(int i = 0; i < broadcastkommunikator.size(); i++) {
			System.out.println(broadcastkommunikator.get(i));
		}	
	}
	
	//Ladung ausgeben 
	public void ladungAusgeben() {
		
		for(int k = 0; k < ladungsverzeichnis.size(); k++) {
			System.out.println(ladungsverzeichnis.get(k).getBezeichnung() + ladungsverzeichnis.get(k).getMenge());
			
			
		}
		
	}
}
	
	



