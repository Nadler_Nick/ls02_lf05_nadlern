package Angestellterverwaltung;

public class AngestellterTest {

	
 //Hauptprogramm
public static void main(String[] args) {
//Zwei Objekte der Klasse Angestellter erzeugen
Angestellter ang1 = new Angestellter();
Angestellter ang2 = new Angestellter();

 // Attribute setzen
ang1.setName("Meier");
ang1.setGehalt(4500);
ang2.setName("Peterson");
ang2.setGehalt(6000);

// Bildschirmausgaben
System.out.println("Name: " + ang1.getName());
System.out.println("Gehalt: " + ang1.getGehalt());
System.out.println("Name: " + ang2.getName());
System.out.println("Gehalt: " + ang2.getGehalt());
}

}
