package Rechteck;

public class Rechteck {

 

    private double seiteA;
    private double seiteB;
    
    public Rechteck(double seiteA, double seiteB)
    {
      setSeiteA(seiteA);
      setSeiteB(seiteB);
    }
    
    public void setSeiteA(double seiteA) {
        if(seiteA > 0)
            this.seiteA = seiteA;
        else
            this.seiteA = 0;
    }
    
    public void setSeiteB(double seiteB) {
        if(seiteB > 0)
            this.seiteB = seiteB;
        else
            this.seiteB = 0;
    }
    
    public double getseiteA() {
        return this.seiteA;
    }
    
    public double getseiteB() {
        return this.seiteB;
    }
    
    public double getFlaeche() {
        return this.seiteA * this.seiteB;
    }
    
    public double getUmfang() {
        return 2 * this.seiteA + 2 * this.seiteB;
    
    }
}